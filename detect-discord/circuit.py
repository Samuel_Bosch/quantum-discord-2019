import numpy as np
import matplotlib.pyplot as plt
from qiskit import QuantumRegister, QuantumCircuit, ClassicalRegister

aq = QuantumRegister(4, 'aq') # ancillary qubits
c = ClassicalRegister(4, 'c')
q = QuantumRegister(8, 'q')

# Circuit renders nicely with barriers, but they should be left out to allow
# optimizations.
def circuit(aq=aq, c=c, q=q, barriers=False):
    # us[i] lists the swaps controlled by qubit i
    us = [[(0, 6), (2, 4), (1, 3), (5, 7)],
          [(0, 6), (1, 3), (5, 7)],
          [(0, 2), (1, 3), (4, 6), (5, 7)],
          [(0, 2), (1, 3), (5, 7)]]

    C = QuantumCircuit(aq, c, q)

    C.h(aq)
    if barriers: C.barrier()
    for i, u in enumerate(us):
        for a, b in u:
            C.cswap(aq[i], q[a], q[b])
        if barriers: C.barrier()

    C.h(aq)
    C.measure(aq, c)

    return C
