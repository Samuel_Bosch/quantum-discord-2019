#!/usr/bin/env python

# Counterexample to eqns (7) and (10), (11) of
# arXiv:1808.10533: Preparing tunable Bell-diagonal states on a quantum computer
# Mauro B. Pozzobom, Jonas Maziero
# v4 (30 Mar 2019)

import numpy as np

def angles(p00, p01, p10, p11):
    theta = 2 * np.arccos(np.sqrt(p00 + p01))
    alpha = 2 * np.arccos(np.sqrt(p00 + p10))
    return theta, alpha

def probs(theta, alpha):
    def p(j, k):
        return (np.cos(theta/2)**2) ** (1 - j) \
             * (np.sin(theta/2)**2) ** j \
             * (np.cos(alpha/2)**2) ** (1 - k) \
             * (np.sin(alpha/2)**2) ** k
    return np.array([p(0, 0), p(0, 1), p(1, 0), p(1, 1)])

# These are inverses of each other only for probabilities of the form
# p(j, k) = p1(j) p2(k)
# where p1(0) + p1(1) = p2(0) + p2(1) = 1.

# Working example
ps_working = (np.array([[0.3], [0.7]]) @ np.array([[0.4, 0.6]])).ravel()
print("Works: {} == {}".format(ps_working, probs(*angles(*ps_working))))

# Failing example
ps_failing = np.array([0.5, 0, 0, 0.5])
print("Fails: {} != {}".format(ps_failing, probs(*angles(*ps_failing))))

# In particular, the Werner states from the paper fail
w = 0.5
ps_werner = np.array(3 * [(1 - w) / 4] + [(1 + 3*w) / 4])
print("Fails (Werner state): {} != {}".format(ps_werner, probs(*angles(*ps_werner))))
